<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = Http::get('https://swapi.dev/api/films/')->json();
        foreach($data['results'] as $key => $value) {
            $data['results'][$key]['favorite'] = Favorite::where('episode_id', $value['episode_id'])
                ->where('favorite', 1)->first() ? true : false;
        }
        return view('movies', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $oMovie = new Favorite();
        $oMovie->episode_id = $id;
        $oMovie->favorite = 1;

        $oMovie->save();
        return redirect()->route('movies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id this is an episode_id and not the id we expected.
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oMovie = Favorite::where('episode_id', $id)->firstOrFail();
        $oMovie->delete();
        return redirect()->route('movies');
    }
}
