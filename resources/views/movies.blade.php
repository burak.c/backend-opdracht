<head>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <h1>Movie list:</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>title</th>
                <th>episode id</th>
                <th>opening crawl</th>
                <th>director</th>
                <th>producer</th>
                <th>release_date</th>
                <th>favorite</th>
                <th></th>
            </tr>
            </thead>
            @foreach($data['results'] as $movie)
                <tr>
                    <td>{{ $movie['title'] }}</td>
                    <td>{{ $movie['episode_id'] }}</td>
                    <td>{{ $movie['opening_crawl'] }}</td>
                    <td>{{ $movie['director'] }}</td>
                    <td>{{ $movie['producer'] }}</td>
                    <td>{{ $movie['release_date'] }}</td>
                    <td>
                        @if($movie['favorite'] == 1)
                            <form action="{{ route('movies.destroy', ['id' => $movie['episode_id']]) }}" method="post">
                                @method('delete')
                                @csrf
                                <input type="hidden" name="episode_id" value="{{ $movie['episode_id'] }}">
                                <button class="btn btn-danger" type="submit">Undo</button>
                            </form>

                        @else
                            <form action="{{ route('movies.favorite', ['id' => $movie['episode_id']]) }}" method="post">
                                @csrf
                                <input type="hidden" name="episode_id" value="{{ $movie['episode_id'] }}">
                                <button class="btn btn-success" type="submit">Favorite</button>
                            </form>
                        @endif
                    </td>
                    <td></td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
